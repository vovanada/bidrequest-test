package main

import (
	"bitbucket.org/vovanada/bidrequest-test/handler"
	"flag"
	"github.com/ipinfo/go-ipinfo/ipinfo"
	"net/http"
)

func main() {
	p := flag.String("port", "8888", "Server port")

	flag.Parse()

	server := handler.NewServer(getIPClient())

	http.HandleFunc("/bid-request", server.BidRequest)
	if err := http.ListenAndServe(":"+*p, nil); err != nil {
		panic(err)
	}
}

func getIPClient() *ipinfo.Client {
	authTransport := ipinfo.AuthTransport{}
	httpClient := authTransport.Client()
	return ipinfo.NewClient(httpClient)
}
