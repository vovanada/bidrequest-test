
## How to use it
* `go mod download`
* `go run main.go --port 8888`
* make post request to 0.0.0.0:8888/bid-request

Example: `curl -H 'Content-Type: application/json' -X POST http://127.0.0.1:8888/bid-request -d '{}'`

## Body example
1. https://wiki.smaato.com/pages/viewpage.action?pageId=9633813
2. https://docs.openx.com/Content/demandpartners/openrtb_bidrequest_video_sample.html#Video2