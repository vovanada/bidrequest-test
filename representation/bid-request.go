package representation

type BidRequest struct {
	ID     string  `json:"id"`
	Device *Device `json:"device"`
	Site   *Site   `json:"site"`
}

type Device struct {
	UserAgent string `json:"ua"`
	IP        string `json:"ip"`
}

type Site struct {
	Page string `json:"page"`
}
