package representation

type BidResponse struct {
	OS         string `json:"os"`
	DeviceType string `json:"device_type"`
	Browser    string `json:"browser"`
	Country    string `json:"country"`
	Domain     string `json:"domain"`
}
