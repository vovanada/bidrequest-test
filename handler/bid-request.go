package handler

import (
	"bitbucket.org/vovanada/bidrequest-test/representation"
	"encoding/json"
	"github.com/mssola/user_agent"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
)

func (s *server) BidRequest(resp http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodPost {
		notSupportMethod(resp)
		return
	}

	bidEntity := &representation.BidRequest{}

	body, err := ioutil.ReadAll(req.Body)

	if err != nil {
		internalErr(resp)
		return
	}

	err = json.Unmarshal(body, &bidEntity)

	if err != nil {
		invalidJSON(resp)
		return
	}

	// build response

	bidResponse := &representation.BidResponse{}

	if bidEntity.Device != nil && bidEntity.Device.UserAgent != "" {
		userAgent := user_agent.New(bidEntity.Device.UserAgent)

		bidResponse.Browser, _ = userAgent.Browser()
		bidResponse.OS = userAgent.OS()
		bidResponse.DeviceType = userAgent.Platform()
	}

	if bidEntity.Device != nil && bidEntity.Device.IP != "" {
		bidResponse.Country, err = s.IPClient.GetCountry(net.ParseIP(bidEntity.Device.IP))

		if err != nil {
			internalErr(resp)
			return
		}
	}

	if bidEntity.Site != nil && bidEntity.Site.Page != "" {
		u, err := url.Parse(bidEntity.Site.Page)
		if err != nil {
			internalErr(resp)
			return
		}

		bidResponse.Domain = u.Host
	}

	writeJSON(resp, bidResponse)
}
