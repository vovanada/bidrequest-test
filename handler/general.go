package handler

import (
	"bitbucket.org/vovanada/bidrequest-test/representation"
	"encoding/json"
	"github.com/ipinfo/go-ipinfo/ipinfo"
	"net/http"
)

func NewServer(IPClient *ipinfo.Client) *server {
	return &server{
		IPClient: IPClient,
	}
}

type server struct {
	IPClient *ipinfo.Client
}

func notSupportMethod(resp http.ResponseWriter) {
	requestErr := representation.Error{
		Message: "Not support method",
	}

	writeJSON(resp, requestErr)
}

func invalidJSON(resp http.ResponseWriter) {
	requestErr := representation.Error{
		Message: "Invalid json",
	}

	writeJSON(resp, requestErr)
}

func internalErr(resp http.ResponseWriter) {
	resp.WriteHeader(http.StatusInternalServerError)
}

func writeJSON(resp http.ResponseWriter, body interface{}) {
	resp.Header().Add("Content-Type", "application/json")
	r, err := json.Marshal(body)
	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}
	_, err = resp.Write(r)

	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}
}
